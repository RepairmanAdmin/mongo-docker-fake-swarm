#!/bin/bash

echo "************************************************************"
echo "Setting up replica set"
echo "************************************************************"

mongo admin --eval "help" > /dev/null 2>&1
RET=$?

while [[ RET -ne 0 ]]; do
  echo "Waiting for MongoDB to start..."
  mongo admin --eval "help" > /dev/null 2>&1
  RET=$?
  sleep 1

  if [[ -f /data/db/mongod.lock ]]; then
    echo "Removing Mongo lock file"
    rm /data/db/mongod.lock
  fi
done

# Login as root and configure replica set
# rs.initiate({ _id: "replica", members: [{ _id: 1, host: "aws-cp-aint-1-m.dornlabs.com:27020" }, { _id: 2, host: "aws-cp-aint-2-w.dornlabs.com:27021" }, { _id: 3, host: "aws-cp-aint-3-w.dornlabs.com:27022" }], settings: { getLastErrorDefaults: { w: "majority", wtimeout: 30000 }}})

# sudo rm -r -f /mnt/mongodata-xfs/data-beta && sudo mkdir /mnt/mongodata-xfs/data-beta