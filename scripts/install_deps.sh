#!/bin/bash
set -e

printf "\n[-] Installing base OS dependencies...\n\n"


# base

apt-get update

apt-get install -y --no-install-recommends ca-certificates openssl numactl wget



printf "\n[-] Generating a replica set keyfile...\n\n"

mkdir mongo-replica-key

openssl rand -base64 741 > $MONGO_KEYFILE

#chown mongodb:mongodb $MONGO_KEYFILE

chmod 400 $MONGO_KEYFILE