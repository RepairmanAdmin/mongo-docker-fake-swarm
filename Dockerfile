FROM mongo:3.4

ENV MONGO_SCRIPTS_DIR scripts_int

ENV MONGO_KEYFILE /mongo-replica-key/keyfile.key

# create user/group


# install dependencies

COPY scripts/install_deps.sh scripts_int/install_deps.sh
COPY scripts/entrypoint.sh scripts_int/entrypoint.sh
COPY scripts/run.sh scripts_int/run.sh


RUN scripts_int/install_deps.sh

# mongod config

#ENV MONGO_ROLE primary 

#During install, we need to set auth to false, 
#then after putting users, you turn it back on

ENV MONGO_AUTH false

ENV MONGO_STORAGE_ENGINE wiredTiger

ENV MONGO_DB_PATH /data/db

ENV MONGO_JOURNALING true

ENV MONGO_REP_SET replica


EXPOSE 27017

WORKDIR scripts_int

ENTRYPOINT ["./entrypoint.sh"]

CMD ["./run.sh"]
